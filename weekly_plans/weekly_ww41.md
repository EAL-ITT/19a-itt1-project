---
Week: 41
Content:  Development phase
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week 41 ITT1 Project - Development

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* All groups are working on tasks 
* Project plan is updated with relevant information and graphical overview  

### Learning goals

* What is a development phase
* Lab report writing as documentation
* What a prototype is

## Deliverables

1. Mandatory weekly meetings with the teachers (this includes minutes of the meeting)
    
    * Agenda is:
        1. Status on project (ie. show closed tasks in gitlab)
        2. Next steps (ie. show next tasks in gitlab)
        3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
        4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
        5. Attendance
        6. Any other business (AOB)

2. Minutes of meeting, from weekly teacher meeting, included in appropiate folder in gitlab project

## Schedule Monday 2019-10-07

* 8:15 Introduction to the day, general Q/A session

    * Phase 2 - Development introduction

        Early prototypes and lab tests
        [Lab report template](http://writeonline.ca/about.php)

* 8:45 Prepare agenda for teacher meeting

* 9:30 Teacher meetings

    All 1. Semester groups and one or more members from 3. Semester groups  

    Timeslot for your weekly meeting with the teachers.  

    Remember to book a time and have an agenda prepared.  

    Add minutes of meeting in your gitlab project  

* 9:30 Group morning meeting

    You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
    Ordinary agenda:
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? Do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.

* 10:00 Hands on time

## Hands-on time

Continue task work

## Comments

Remember to ask questions if you have any!

\pagebreak