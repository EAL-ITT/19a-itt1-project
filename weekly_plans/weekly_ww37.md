---
Week: 37
Content:  Project startup
Material: See links in weekly plan
Initials: NISI/ILES
---

# Week 37 ITT1 Project - Idea and research

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Groups have been formed
* Gitlab project created

### Learning goals

* What a project is
* Project management
* Risk management

## Deliverables

* Teachers added to gitlab project 

## Schedule

### Monday 2019-09-09

* 08:15 Introduction to the project lectures 

    [google slides presentation](https://docs.google.com/presentation/d/1gKS9Mw4nWfEJnkdrwEexPpExaU0urh0Apz38lcwq6TY/edit?usp=sharing)

* 10:00 (ish) Exercise 0

* 11:00 [Prosa](https://www.prosa.dk/english/) presentation

* 12:15 Exercise 1 - group part

* 13:00 Study start test 1st attempt, room A0.29

* 14:15 Exercise 1 - meeting on class

## Hands-on time

* Exercise 0: gitlab project

    Setup a blank [gitlab project](https://docs.gitlab.com/ee/gitlab-basics/create-project.html)  

    Name the project: itt1_project_`project_topic_`_`groupname`

    **example** itt1_project_wireless_charging_robot_G1

    Remember to include teachers [npes](https://gitlab.com/npes) and [ilias-gitlab](https://gitlab.com/ilias-gitlab)
    
* Exercise 1: pre-mortem meeting

    In your group do a [pre-mortem meeting](https://hbr.org/2007/09/performing-a-project-premortem)

    **Group work**

    1. (20 min) Imagine that we fail in implementing the project. In the group, come up with as many reasons as possible for that to have happened.

    2. Select the top 5 most relevant.
        relevant might be equivalent to most likely in this context
        exclude stuff we don't have any control over, e.g. meteor showers.

    **Meeting on class**

    3. Write them on the whiteboard

    4. (30 min) On class, we will discuss how to prevent the problems from arizing, including solutions and who is responsible.

    **Group work**

    5. Note in a document the risks, solutions and responsibility and upload it to your groups gitlab project

## Comments

If you forgot how to interact with gitlab through git, consult the codelab from [programming class](https://npes.gitlab.io/claat-generator/gitlab_daily_workflow/index.html#0)  

\pagebreak