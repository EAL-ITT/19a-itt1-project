---
title: 'ITT1 Project'
subtitle: 'Exercises'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Ilias Esmati \<iles@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: 'ITT1 project, exercises'
---


Introduction
====================

This document is a collection of exercises. They are associated with the weekly plans. 

References

* [Generic Project plan](https://eal-itt.gitlab.io/19a-itt1-project/project_plan_for_students.pdf)
* [Weekly plans](https://eal-itt.gitlab.io/19a-itt1-project/19A_ITT1_weekly_plans.pdf)



